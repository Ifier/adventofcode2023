#include <stdio.h>

//lookup table, features both the translated value and
//the value times ten
struct lookup_entry
{
    unsigned char m_output;
    unsigned char m_output_times_10;
    unsigned char m_input_size;
    char          m_input[5];
};

//in the case that two bytes are needed to distinquish a lookup
//(i.e. two and three), the third bit of the second letter is
//sufficent to distinquish them, and can be put into the 8th
//bit of the 7 bit ascii code
#define LOOKUP_CHAR(str) str[0] | (((str[1] >> 2) & 1) << 7)
#define MAKE_LOOKUP_ENTRY(str,digit) { digit, (unsigned char)(digit * 10), sizeof(str)-1, str }

#define WORD_LOOKUP_ENTRY(str,digit) [LOOKUP_CHAR(str)] = MAKE_LOOKUP_ENTRY(str,digit)
#define CHAR_LOOKUP_ENTRY(str,digit) [LOOKUP_CHAR(str)] = MAKE_LOOKUP_ENTRY(str,digit), \
                                     [LOOKUP_CHAR(str) | 128] = MAKE_LOOKUP_ENTRY(str,digit)

_Alignas(8) static const struct lookup_entry lookup_table[256] =
{
    //special case for newline
    CHAR_LOOKUP_ENTRY("\n", 255),

    CHAR_LOOKUP_ENTRY("0", 0),
    CHAR_LOOKUP_ENTRY("1", 1),
    CHAR_LOOKUP_ENTRY("2", 2),
    CHAR_LOOKUP_ENTRY("3", 3),
    CHAR_LOOKUP_ENTRY("4", 4),
    CHAR_LOOKUP_ENTRY("5", 5),
    CHAR_LOOKUP_ENTRY("6", 6),
    CHAR_LOOKUP_ENTRY("7", 7),
    CHAR_LOOKUP_ENTRY("8", 8),
    CHAR_LOOKUP_ENTRY("9", 9),

	WORD_LOOKUP_ENTRY("zero", 0),
    WORD_LOOKUP_ENTRY("one", 1),
    WORD_LOOKUP_ENTRY("two", 2),
    WORD_LOOKUP_ENTRY("three", 3),
    WORD_LOOKUP_ENTRY("four", 4),
    WORD_LOOKUP_ENTRY("five", 5),
    WORD_LOOKUP_ENTRY("six", 6),
    WORD_LOOKUP_ENTRY("seven", 7),
    WORD_LOOKUP_ENTRY("eight", 8),
    WORD_LOOKUP_ENTRY("nine", 9),
};

//keep an unsigned long long of padding at the end that isn't
//consumed on a scan through to make sure we don't miss numbers
//that are spread over two packets
#define BUFFER_PADDING sizeof(unsigned long long)
#define BUFFER_SIZE 256 + BUFFER_PADDING
#define BUFFER_MAIN_SIZE BUFFER_SIZE - BUFFER_PADDING

//refills the buffer and pads with zeros, returns amount of genuine characters in the buffer
inline size_t refill_buffer(FILE* fd, char* buffer)
{
    //move the end of the buffer to the start of the buffer
    unsigned long long* buffer_last = (unsigned long long*)(buffer + BUFFER_MAIN_SIZE);
    unsigned long long* buffer_first = (unsigned long long*)buffer;
    (*buffer_first) = (*buffer_last);

    size_t bytes_read = fread(buffer + BUFFER_PADDING, 1, BUFFER_MAIN_SIZE, fd);
    size_t bytes_in_buffer = bytes_read;
    
    //if the read was only partially successful
    if (bytes_read != BUFFER_MAIN_SIZE)
    {
    	bytes_in_buffer += BUFFER_PADDING;
    
        //if the buffer didn't end with a newline, add one
        //to ensure the last number is flushed to sum
        if (buffer[bytes_read + BUFFER_PADDING] != '\n')
        {
        	buffer[++bytes_read + BUFFER_PADDING] = '\n';
        }
        //pad buffer with zeros
        do
        {
            buffer[bytes_read + BUFFER_PADDING] = 0;
        }
        while (++bytes_read != BUFFER_MAIN_SIZE);
    }

    return bytes_in_buffer;
}
inline unsigned char confirm_digit(struct lookup_entry* entry, const char* input)
{
    unsigned long long in = *((const unsigned long long*)input);
    unsigned long long mask = (1ull << (entry->m_input_size * 8)) - 1;
    
    unsigned long long expected = *((unsigned long long*)entry);
    return (in & mask) == (expected >> (8 * 3));
}
inline char* scan_buffer(char* output, const char* input, int* sum)
{
    const char* const input_end = input + BUFFER_MAIN_SIZE;

    do
    {
        struct lookup_entry entry = lookup_table[LOOKUP_CHAR(input)];

        //special case for newline
        if (entry.m_output == 255)
        {
            //reset output buffer to start and sum first and
            //last numbers
            output = (char*)((unsigned long long)output & ~1ull);
            *sum += output[0] + output[1];
            
            //reset the buffer
            output[0] = 0;
            output[1] = 0;
        }
        else if (entry.m_input_size != 0 &&
                 confirm_digit(&entry, input))
        {
            *output = entry.m_output_times_10;
            
            //or the output cursor address by 1 so it's only incremented
            //on the first number we hit (and therefore keeps the first
            //output as 10 times the first number hit)
            output = (char*)((unsigned long long)output | 1);
            
            *output = entry.m_output;
        }
    }
    while (++input != input_end);

    return output;
}
int main(int argc, const char** argv)
{
    //validate number of open args
    if (argc != 2)
    {
    	printf("wrong number of arguments! Expected only a file name");
        return -1;
    }
    
    //open file in read mode
    FILE* fd = fopen(argv[1], "r");

    char input_buffer[BUFFER_SIZE];
    //ensure that the lowest bit of the address is zero
    _Alignas(2) char output_buffer[2];
    
    int sum = 0;
    size_t bytes_read;
    //read in the full buffer
    if ((bytes_read = fread(input_buffer, 1, BUFFER_SIZE, fd)) > 0)
    {
        //scan until we're just left with the padding on the last iteration
        char* output = output_buffer;
        do
        {
            output = scan_buffer(output, input_buffer, &sum);
        }
        while ((bytes_read = refill_buffer(fd, input_buffer)) > BUFFER_PADDING);
        
        //final readout from buffer padding
        output = scan_buffer(output, input_buffer, &sum);
    }

    //print calibration value
	printf("calibration value: %i\n", sum);
    
    return 0;
}