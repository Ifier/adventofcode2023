#include <stdio.h>
#include <stdlib.h>

struct game_result
{
    int          m_ID;
    unsigned int m_reds;
    unsigned int m_greens;
    unsigned int m_blues;
};

//keep an unsigned long long of padding at the end that isn't
//consumed on a scan through to make sure we don't miss numbers
//that are spread over two packets
#define BUFFER_PADDING sizeof(unsigned long long)
#define BUFFER_SIZE 256 + BUFFER_PADDING
#define BUFFER_MAIN_SIZE BUFFER_SIZE - BUFFER_PADDING

#define MAX_GAMES 127

//refills the buffer and pads with zeros, returns amount of genuine characters in the buffer
size_t refill_buffer(FILE* fd, char* buffer)
{
    //move the end of the buffer to the start of the buffer
    unsigned long long* buffer_last = (unsigned long long*)(buffer + BUFFER_MAIN_SIZE);
    unsigned long long* buffer_first = (unsigned long long*)buffer;
    (*buffer_first) = (*buffer_last);

    size_t bytes_read = fread(buffer + BUFFER_PADDING, 1, BUFFER_MAIN_SIZE, fd);
    size_t bytes_in_buffer = bytes_read;
    
    //if the read was only partially successful
    if (bytes_read != BUFFER_MAIN_SIZE)
    {
    	bytes_in_buffer += BUFFER_PADDING;
    
        //if the buffer didn't end with a newline, add one
        //to ensure the last number is flushed to sum
        if (buffer[bytes_read + BUFFER_PADDING] != '\n')
        {
        	buffer[++bytes_read + BUFFER_PADDING] = '\n';
        }
        //pad buffer with zeros
        do
        {
            buffer[bytes_read + BUFFER_PADDING] = 0;
        }
        while (++bytes_read != BUFFER_MAIN_SIZE);
    }

    return bytes_in_buffer;
}
struct game_result* scan_buffer(struct game_result* output, const char* input)
{
    const char* const input_end = input + BUFFER_MAIN_SIZE;

    do
    {
        //positive ID represents an output currently undergoing ID accumulation.
        //IDs are one above the actual value to account for this
        if (output->m_ID > 0)
        {
            //parse until find a number
            while ((*input < '0' || *input > '9') && ++input != input_end)
            {
            }
            if (input != input_end)
            {
                output->m_ID -= 1;
                
                while (*input >= '0' && *input <= '9' && input != input_end)
                {
                    output->m_ID *= 10;
                    output->m_ID += ((*input++) & 0xF);
                }
                
                output->m_ID += 1;
                
                //make sure we don't false positive in the case that we 
                if (*input < '0' || *input > '9')
                {
                    output->m_reds = 0;
                    output->m_greens = 0;
                    output->m_blues = 0;
                    
                    //prepare accululator game
                    ++output;
                    output->m_ID = -1;
                    output->m_reds = 0;
                    output->m_greens = 0;
                    output->m_blues = 0;
                }
            }
        }
        //negative IDs represent accumulator outputs (i.e. not actual games) which
        //are currently parsing a colour. ID represents the compilment of the accumulated
        //colour value
        else if (output->m_ID < 0)
        {
            output->m_ID = ~output->m_ID;
        
            //parse until find a number
            while ((*input < '0' || *input > '9') && ++input != input_end)
            {
            }
            //parse the number
            while (*input >= '0' && *input <= '9' && input != input_end)
            {
                output->m_ID *= 10;
                output->m_ID += ((*input++) & 0xF);
            }
            
            //read colour time. Would need modification for new colour and requires
            //that every colour have exactly 1 space between the number and colour
            if (*input < '0' || *input > '9')
            {
                unsigned long long colour = *((unsigned long long*)input);
                
                //red
                if ((colour & 0xFFFFFF00) == 0x64657200)
                {
                    output->m_reds += output->m_ID;
                }
                //green
                else if ((colour & 0xFFFFFFFFFF00) == 0x6E6565726700)
                {
                    output->m_greens += output->m_ID;
                }
                //blue
                else
                {
                    output->m_blues += output->m_ID;
                }
                
                //look for terminator
                output->m_ID = 0;
            }
            else
            {
                //reset so still negative for next iteration
                output->m_ID = ~output->m_ID;
            }
        }
        //IDs of exactly zero represents accumulator outputs (i.e. not actual games) which
        //have processed a colour and are awaiting a terminator, representing the presence of
        //another colour in the same set, a new set, or a new game.
        else
        {
            //parse until terminator
            while (*input != ';' && *input != '\n' && *input != ',' && ++input != input_end)
            {
            }
            
            struct game_result* main = output - 1;
            
            //game complete, tally results
            if (*input == ';' || *input == '\n')
            { 
                main->m_reds   = (main->m_reds   > output->m_reds)   ? main->m_reds   : output->m_reds;
                main->m_greens = (main->m_greens > output->m_greens) ? main->m_greens : output->m_greens;
                main->m_blues  = (main->m_blues  > output->m_blues)  ? main->m_blues  : output->m_blues;
                
                //reset totals
                output->m_reds = 0;
                output->m_greens = 0;
                output->m_blues = 0;
            }
            //setup new game if end of line
            if (*input == '\n')
            {
                output->m_ID = 1;
            }
            //just reset to finding the next number
            else if (input != input_end)
            {
                output->m_ID = -1;
            }
        }
    }
    while (input != input_end);

    return output;
}
int parse_number(const char* str)
{
    int result = 0;
    while (*str != 0)
    {
        result *= 10;
        result += (*str++) & 0xF;
    }
    return result;
}
int main(int argc, const char** argv)
{
    //validate number of open args
    if (argc != 5)
    {
    	printf("wrong number of arguments! Expected a file name, and then values for red, green and blue");
        return -1;
    }
    
    //open file in read mode
    FILE* fd = fopen(argv[1], "r");

    char input_buffer[BUFFER_SIZE];
    struct game_result game_buffer[MAX_GAMES + 1];
    struct game_result* game_stack = game_buffer;
    game_buffer[0].m_ID = 1;
    game_buffer[0].m_reds = 0;
    game_buffer[0].m_blues = 0;
    game_buffer[0].m_greens = 0;
    
    size_t bytes_read;
    //read in the full buffer
    if ((bytes_read = fread(input_buffer, 1, BUFFER_SIZE, fd)) > 0)
    {
        //scan until we're just left with the padding on the last iteration
        do
        {
            game_stack = scan_buffer(game_stack, input_buffer);
        }
        while ((bytes_read = refill_buffer(fd, input_buffer)) > BUFFER_PADDING);
        
        //final readout from buffer padding
        game_stack = scan_buffer(game_stack, input_buffer);
    }
    //there may be a blank game at the bottom of the stack, but since
    //it's blank it shouldn't matter
    
    unsigned int maximum_red   = parse_number(argv[2]);
    unsigned int maximum_green = parse_number(argv[3]);
    unsigned int maximum_blue  = parse_number(argv[4]);
    
    int total = 0;
    int power_total = 0;
    
    struct game_result* current_game = game_buffer;
    while (current_game != game_stack)
    {
        power_total += (current_game->m_reds * current_game->m_greens * current_game->m_blues);
            
        if (current_game->m_reds <= maximum_red &&
            current_game->m_greens <= maximum_green &&
            current_game->m_blues <= maximum_blue)
        {
            total += current_game->m_ID - 1;
        }
        ++current_game;
    }
    
    printf("matching game ID total: %i, game power total: %u\n", total, power_total);
    
    return 0;
}
